<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Spatie\MediaLibrary\Exceptions\FileCannotBeAdded;
use Spatie\MediaLibrary\Exceptions\FileCannotBeAdded\InvalidBase64Data;
use Spatie\MediaLibrary\Exceptions\MediaCannotBeDeleted;
use App\Http\Controllers\MediaController;
use App\Models\Media;
use App\Models\Post;
use App\User;

class FilemanagerController extends MediaController
{
    private $collection;

    private $collections = [
        1 => 'images',
        2 => 'files',
        3 => 'medias'
    ];

    private $mimeTypes = [
        'images' => [
            'image/apng', 'image/png', 'image/gif', 'image/jpeg', 'image/svg+xml', 'image/webp'
        ],
        'medias' => [
            'audio/wave', 'audio/wav', 'audio/x-wav', 'audio/x-pn-wav', 'audio/webm', 'video/webm',
            'audio/ogg', 'video/ogg', 'application/ogg', 'video/mp4', 'audio/mpeg', 'video/x-flv',
            'application/x-mpegURL', 'video/MP2T', 'video/3gpp', 'video/quicktime', 'video/x-msvideo',
            'video/x-ms-wmv'
        ],
    ];

    public function __construct()
    {
        parent::__construct();
        $this->collection = \request('type') && array_key_exists((int)\request('type'), $this->collections) ? $this->collections[(int)\request('type')] : 'all';
    }

    public function index()
    {
        return view('filemanager.index');
    }

    public function dialog()
    {
        return view('MediaManager::media');
    }

    public function upload(Request $request, $user_id = null, $file_name = null)
    {
        $user_id = is_null($user_id) ? auth('web')->user()->id : $user_id;
        $user = User::find($user_id);
        $result = [];
        if (is_array($request->file('file'))) {
            foreach ($request->file('file') as $file) {
                $user->addMedia($file)->toMediaCollection($this->getFileTypeByMimeType($file->getMimeType()));
                $result[] = [
                    'success' => true,
                    'file_name' => $file->getClientOriginalName(),
                ];
            }
        } else {
            $file_name = is_null($file_name) ? $request->file('file')->getClientOriginalName() : $file_name;
            $user->addMediaFromRequest('file')
                ->setName($file_name)
                ->toMediaCollection($this->getFileTypeByMimeType($request->file('file')->getMimeType()));
            return [
                'success' => true,
                'file_name' => $file_name,
            ];
        }
        return $result;
    }

    public function uploadEditedImage(Request $request)
    {
        $data = explode(',', $request->data)[1];
        $user = User::find(auth('web')->user()->id);
        $original = $request->name;
        $name_only = pathinfo($original, PATHINFO_FILENAME) . '_' . $this->getRandomString();
        $ext_only = pathinfo($original, PATHINFO_EXTENSION);
        $file_name = "$name_only.$ext_only";
        try {
            $user->addMediaFromBase64($data)
                ->setFileName($file_name)
                ->toMediaCollection($this->getFileTypeByMimeType($request->mime_type));
            $result = [
                'success' => true,
                'message' => $file_name,
            ];
        } catch (InvalidBase64Data $e) {
            $result = [
                'success' => false,
                'message' => "\"$file_name\" " . $e->getMessage(),
            ];
        } catch (FileCannotBeAdded $e) {
            $result = [
                'success' => false,
                'message' => "\"$file_name\" " . $e->getMessage(),
            ];
        }

        return response($result);
    }

    public function deleteItem(Request $request)
    {
        $user = User::find(auth('web')->user()->id);
        $result = [];
        if (is_array($request->deleted_files)) {
            foreach ($request->deleted_files as $file) {
                $defaults = [];
                $this->drop($file['id'], $user);
                if (array_key_exists('name', $file)) {
                    $defaults = [
                        'name' => $file['name'],
                        'path' => $file['storage_path'],
                    ];
                }
                $result[] = array_merge($defaults, ['success' => true]);
            }
        } else {
            $this->drop($request->input('deleted_files'), $user);
            return ['success' => true];
        }

        return $result;
    }

    public function renameItem(Request $request)
    {
        $file = $request->file;
        $original = $this->cleanName($request->new_filename);
        $name_only = pathinfo($original, PATHINFO_FILENAME);
        $ext_only = pathinfo($original, PATHINFO_EXTENSION);
//        $old_filename = pathinfo($file['name'], PATHINFO_FILENAME);
        $new_filename = "{$name_only}.{$ext_only}";
        $media = Media::find($file['id']);

        $media->name = $name_only;
        $media->file_name = $new_filename;

        $postMedia = Media::where('manipulations->base_image->id', $file['id'])->first();
        if ($postMedia instanceof Media) {
            $postMedia->name = $name_only;
            $postMedia->file_name = $new_filename;
            $postMedia->save();
        }

        $media->save();

        return compact('message', 'new_filename');
    }

    protected function getData($dir)
    {
//        $user = \request('user') ? User::findOrFail(\request('user')) : auth('web')->user();
//        $userMedias = $user->getMedia($collection)->sortByDesc('created_at')->paginate()->setPageName('user_media_page');
        if ($this->collection === 'all')
            $medias = Media::orderByDesc('created_at')->cursor()
                ->filter(function ($value, $key) {
                    return $value->model_type === User::class;
                });
        else
            $medias = Media::whereCollectionName($this->collection)->orderByDesc('created_at')->cursor()
                ->filter(function ($value, $key) {
                    return $value->model_type === User::class;
                });

        $list = [];

        foreach ($medias as $media) {
            $list[] = [
                'id' => $media->id,
                'name' => $media->file_name,
                'type' => $media->mime_type,
                'path' => $media->getUrl(),
                'storage_path' => $media->getPath(),
                'size' => $media->size,
                'visibility' => '0777',
                'last_modified' => $media->updated_at->timestamp,
                'last_modified_formated' => $media->updated_at->format('Y-d-m'),
            ];
        }

        return $list;
    }

    private function getFileTypeByMimeType(string $mimeType)
    {
        foreach ($this->mimeTypes as $key => $value) {
            if (in_array($mimeType, $value))
                return $key;
        }
        return 'files';
    }

    private function drop($file_id, User $user)
    {
        // remove post media
        $media = Media::where('manipulations->base_image->id', $file_id)->first();
        if ($media instanceof Media) {
            $post = Post::find($media->model_id);
            try {
                $post->deleteMedia($media->id);
            } catch (MediaCannotBeDeleted $e) {
            }
        }

        // remove user media
        try {
            $user->deleteMedia($file_id);
        } catch (MediaCannotBeDeleted $e) {
        }
    }

    public static function doUpload(Request $request, $user_id = null, $file_name = null)
    {
        return (new FilemanagerController)->upload($request, $user_id, $file_name);
    }
}
