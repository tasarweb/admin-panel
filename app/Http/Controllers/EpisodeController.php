<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EpisodeController extends Controller
{
    public function index()
    {
        return redirect(route('course.index'));
    }

    public function create()
    {
        if (!request()->has('course'))
            return redirect()->back();

        $course_id = request()->course;
        return view('course.episode-create-edit', compact('course_id'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'video' => 'required'
        ]);

        $response = app('http-auth')->attach(
            'video', fopen($request->video, 'r')
        )->post(config('admin.course_create_url'), $request->except('video'));

        if (!$response->ok()) return '';

        return redirect(route('course.index'));
    }

    public function show($course_slug)
    {
        $response = app('http-auth')->get(sprintf(config('admin.course_get_url'), $course_slug));

        if (!$response->ok()) return '';

        $course = json_decode($response->body(), true);

        return view('course.episode', compact('course'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getVideo(Request $request)
    {
	$response = app('http-auth')->post(sprintf(config('admin.episode_download_url'), $request->input('e')));

	return response($response->body());
    }
}
