<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionController extends Controller
{
    public function index()
    {
        $permissions = Permission::orderByDesc('id')->paginate(10);

        return view('permission.index', compact('permissions'));
    }

    public function create()
    {
        $roles = Role::all();
        $permissions = Permission::all();

        return view('permission.create-edit', compact('roles', 'permissions'));
    }

    public function store(Request $request)
    {
        $input = [
            'name' => $request->name,
            'guard_name' => $request->guard_name
        ];

        $permission = Permission::create($input);

        if ($permission instanceof Permission) {

            if ($request->has('roles')) {

                foreach ($request->input('roles') as $role) {
                    $roleModel = Role::findById($role);
                    $permission->assignRole($roleModel);
                }
            }

            return redirect(route('permission.index'))->with(['ok' => true, 'message' => 'Permission Created Successfully']);
        }
        return redirect()->back()->with(['ok' => false, 'message' => 'Error']);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
