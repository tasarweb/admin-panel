<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::all();
        return view('admin.role.index', compact('roles'));
    }

    public function create()
    {
        $roles = Role::all();
        $permissions = Permission::all();

        return view('role.create-edit', compact('roles', 'permissions'));
    }

    public function store(Request $request)
    {
        $input = [
            'name' => $request->name,
            'guard_name' => $request->guard_name
        ];

        $role = Role::create($input);

        if ($role instanceof Role) {

            if ($request->has('permissions')) {

                foreach ($request->input('permissions') as $permission) {
                    $permissionModel = Permission::findById($permission);
                    $role->givePermissionTo($permissionModel);
                }
            }

            return redirect(route('role.index'))->with(['ok' => true, 'message' => 'Permission Created Successfully']);
        }
        return redirect()->back()->with(['ok' => false, 'message' => 'Error']);
    }

    public function show($id)
    {
        return abort(404);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
