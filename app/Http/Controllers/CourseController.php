<?php

namespace App\Http\Controllers;

use App\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Http;

class CourseController extends Controller
{

    public function index()
    {

        $response = app('http-auth')->get(config('admin.course_list_url'));

        $courses = json_decode($response->body(), true);

        $courses = collect($courses)->paginate();

        return view('course.index', compact('courses'));
    }


    public function create()
    {
        $categories = app('http-auth')->get(config('admin.course_categories_list_url'));
        $tags = app('http-auth')->get(config('admin.course_tags_list_url'));

        $categories = json_decode($categories->body(), true);
        $tags = $tags->body();


        return view('course.create-edit', compact('categories', 'tags'));
    }


    public function store(Request $request)
    {
        $request->merge(['tags' => json_decode($request->input('tags'), true)]);


	if($request->hasFile('image')) {
	     $data = [
		'multipart' => [
			[
			    'name' => 'image',
			    'contents' => fopen( $request->file('image')->getPathname(), 'r' ),
			],
		    ],
	     ];
	     //$data = array_merge($temp, $request->except(['image']));
	}
	else {
	     $data = $request->all();
	}


        $response = app('http-auth')
            ->post(config('admin.course_create_url'), [
    'multipart' => [
        [
            'name'     => 'foo',
            'contents' => 'data',
            'headers'  => ['X-Baz' => 'bar']
        ],
        [
            'name'     => 'baz',
            'contents' => fopen( $request->file('image')->getPathname(), 'r' ),
        ],
    ]
]);

return $response->body();

        if (!$response->ok()) {
            notify()->error('Course can not be saved!');
            return redirect()->back()->withErrors($response->body())->withInput();
        }
        smilify('success', 'Post Successfully Updated');
        return redirect(route('course.index'));
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {

        return response(['ok' => false, 'message' => 'Error']);
    }
}
