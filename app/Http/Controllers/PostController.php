<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Spatie\MediaLibrary\Exceptions\MediaCannotBeDeleted;
use App\Events\PostPublished;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use Illuminate\Support\Facades\Http;

class PostController extends Controller
{
    public function index()
    {
        $response = Http::get(config('admin.api_url') . '/v1/course');

        $posts = json_decode($response->body(), true);

        return view('post.index', compact('posts'));
    }

    public function create()
    {
        $categories = Category::all();
        $tags = Tag::pluck('name');
        $status = $this->getPostStatus();
        $types = $this->getPostTypes();
        return view('post.create-edit', compact('categories', 'status', 'types', 'tags'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'slug' => [Rule::unique('posts', 'slug')]
        ]);

        $event = $request->input('status') === config('tasarblog.post.publish_status');

        $input = [
            'title' => $request->input('title'),
            'slug' => $request->input('slug'),
            'content' => $request->input('content'),
            'status' => $request->input('status'),
            'type' => $request->input('type')
        ];


        if ($request->input('featured'))
            $input['featured'] = $request->input('featured');


        $post = Post::create($input);

        if ($post instanceof Post) {

            $post->syncCategories($request->input('category_id'));
            if ($request->input('tags'))
                $post->syncTags(explode(',', $request->input('tags')));
            $this->addImage($request, $post);

            if ($event) event(new PostPublished($post->user, $post));
            smilify('success', 'Post Successfully Updated');
            return redirect(route('post.index'));

        }

        notify()->error('Post can not be saved!');
        return redirect()->back();
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $post = Post::find($id);
        if (!($post instanceof Post)) {
            notify()->error('Post Not Found');
            return redirect(route('post.index'));
        }
        $categories = Category::all();
        $tags = Tag::pluck('name');
        $status = $this->getPostStatus();
        $types = $this->getPostTypes();
        return view('post.create-edit', compact('post', 'categories', 'status', 'types', 'tags'));
    }

    public function update(PostRequest $request, $id)
    {
        $post = Post::find($id);
        if (!($post instanceof Post)) {
            notify()->error('Post Not Found');
            redirect()->back();
        }

        $this->validate($request, [
            'slug' => [Rule::unique('posts', 'slug')->ignore($post->id)]
        ]);

        $event = $request->input('status') === config('tasarblog.post.publish_status');


        $post->title = $request->input('title');
        $post->slug = $request->input('slug');
        $post->content = $request->input('content');
        $post->status = $request->input('status');
        $post->type = $request->input('type');
        if ($request->input('featured'))
            $post->featured = $request->input('featured');
        $post->syncCategories($request->input('category_id'));
        if ($request->input('tags'))
            $post->syncTags(explode(',', $request->input('tags')));
        $this->addImage($request, $post);

        if ($post->save()) {
            if ($event) event(new PostPublished($post->user, $post));
            smilify('success', 'Post Successfully Updated');
            return redirect(route('post.index'));
        }
        notify()->error('Post can not be saved!');
        return redirect()->back();
    }

    public function destroy($id)
    {
        $post = Post::find($id);
        $post->categories()->sync([]);
        $post->syncTags([]);
        $post->delete();
        return response(['ok' => true, 'message' => 'Post Successfully Deleted']);
    }

    private function addImage(PostRequest $request, Post $post)
    {
        if ($request->hasFile('image')) {
            $this->removeFirstMedia($post);
            $image = $post->addMediaFromRequest('image')
                ->withResponsiveImages()
                ->toMediaCollection('images');
            $image->copy(User::find(auth()->user()->id), 'images');

        } elseif ($request->input('image_id') && $this->removeFirstMedia($post, $request->input('image_id')))
            $post->copyMedia(Media::find($request->input('image_id'))->getPath())
                ->withResponsiveImages()
                ->withManipulations([
                    'base_image' => [
                        'id' => $request->input('image_id')
                    ]
                ])
                ->toMediaCollection('images');
    }

    private function removeFirstMedia(Post $post, $imageId = ''): bool
    {
        try {
            if ($imageId !== $post->getFirstMedia('images')->id) {
                $post->deleteMedia($post->getFirstMedia('images')->id);
                return true;
            }
            return false;
        } catch (MediaCannotBeDeleted $e) {
            \Log::error($e->getMessage());
            return false;
        } catch (\ErrorException $e) {
            \Log::error($e->getMessage());
            return true;
        }
    }

    private function getPostStatus()
    {
        return [
            'PENDING' => __('Pending'),
            'PUBLISHED' => __('Published'),
            'DRAFT' => __('Draft'),
            'REJECTED' => __('Rejected')
        ];
    }

    private function getPostTypes()
    {
        return [
            'TEXT' => __('Text'),
            'GALLERY' => __('Gallery'),
            'VIDEO' => __('Video'),
            'COURSE' => __('Course'),
            'LINK' => __('Link')
        ];
    }
}
