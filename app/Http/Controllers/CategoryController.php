<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
	$response = app('http')->get(config('admin.course_categories_list_url') . '?panel=1');
	
	if(!$response->ok()) return '';
	
        $categories = json_decode($response->body(), true);

	$categories = collect($categories)->paginate();

        return view('category.index', compact('categories'));
    }

    public function create()
    {
        $response = app('http')->get(config('admin.course_categories_list_url'));
	
	if(!$response->ok()) return '';
	
        $categories = json_decode($response->body(), true);

        return view('category.create-edit', compact('categories'));
    }

    public function store(Request $request)
    {
        $response = app('http-auth')->post(config('admin.course_create_category_url'), $request->all());

	if (!$response->ok()) {
            notify()->error('Category can not be saved!');
            return redirect()->back()->withErrors($response->body())->withInput();
        }

	smilify('success', 'Category successfully Created');
	return redirect(route('category.index'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
