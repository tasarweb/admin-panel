<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class AuthenticationController extends Controller
{

    public function login()
    {

	$response = app('http')->get(config('admin.api_url') . '/v1/captcha/math');

        $body = json_decode($response->body(), true);


	$key = $body['key'];
	$captcha = $body['img'];

	return view('auth.login', compact('captcha', 'key'));
    }

    public function doLogin(Request $request)
    {

        $fields = [
	    'key' => $request->input('key'),
	    'captcha' => $request->input('captcha'),
            'login_name' => $request->input('email'),
            'password' => $request->input('password')
        ];

        $response = app('http')->post(config('admin.api_url') . '/login', $fields);

        $body = json_decode($response->body(), true);

        if ($response->status() !== 200) {
            return redirect()->back()->withErrors(['message' => $body['message']]);
        }

        $token = $body['token'];

        $user = app('http')->withToken($token)->get(config('admin.api_url') . '/v1/user/me');

        if ($user->status() !== 200) {
            return redirect()->back()->withErrors(['message' => 'error']);
        }

        $userBody = json_decode($user->body(), true);

        Auth::loginUsingId($userBody['id'], true);

        setcookie('auth-token', $token);

        return redirect()->route('dashboard');
    }

    public function register(Request $request)
    {

    }

    public function logout(Request $request)
    {

    }
}
