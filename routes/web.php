<?php

//Auth::routes();
Route::post('/login', 'AuthenticationController@doLogin')->middleware('guest')->name('doLogin');
Route::get('/login', 'AuthenticationController@login')->name('login')->middleware('guest');
Route::post('/register', 'AuthenticationController@register')->middleware('guest')->name('doRegister');
Route::view('/register', 'auth.register')->name('register')->middleware('guest');
Route::post('/logout', 'AuthenticationController@logout')->middleware('auth')->name('logout');

Route::group(['middleware' => 'admin'], function () {
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::post('/get/video', 'EpisodeController@getVideo')->name('get.video');
    Route::resources([
        'category' => 'CategoryController',
        'role' => 'RoleController',
        'permission' => 'PermissionController',
        'comment' => 'CommentController',
        'user' => 'UserController',
        'post' => 'PostController',
        'course' => 'CourseController',
        'episode' => 'EpisodeController',
        'word' => 'SuggestedWordController',
    ]);
});


//Route::get('/filemanager', 'FilemanagerController@dialog')->name('admin.filemanager.dialog');
//ctf0\MediaManager\MediaRoutes::routes();
