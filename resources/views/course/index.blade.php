@extends('layouts.main')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col col-8">
                <div class="card">
                    <div class="card-header text-center">
                        <strong>Courses</strong>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead class="thead-dark">
                                <tr>
                                    <th>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="all-items" class="custom-control-input"
                                                   v-model="allItems">
                                            <label class="custom-control-label" for="all-items"></label>
                                        </div>
                                    </th>
                                    <th class="text-center">Title</th>
                                    <th class="text-center">Author</th>
                                    <th class="text-center">Episodes</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Featured</th>
                                    <th class="text-center">Published</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($courses as $course)
                                    <tr>
                                        <th class="checkbox">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" value="{{ $course['id'] }}"
                                                       name="category[]" id="{{ $course['id'] }}"
                                                       class="custom-control-input">
                                                <label class="custom-control-label" for="{{ $course['id'] }}"></label>
                                            </div>
                                        </th>
                                        <td>{{ $course['title'] }}</td>
                                        <td>
                                            <a href="{{ route('user.show', $course['author']['username']) }}"
                                               target="_blank">
                                                {{ $course['author']['username'] }}
                                            </a>
                                        </td>
                                        <td>{{ $course['episodes_count'] }}</td>
                                        <td>{{ $course['status'] }}</td>
                                        <td>{{ $course['featured'] }}</td>
                                        <td>{{ \Carbon\Carbon::parse($course['updated_at'])->diffForHumans() }}</td>
                                        <td class="text-center">
                                            <a href="{{ route('episode.show', $course['slug']) }}"
                                               title="{{ __('Episodes') }}" class="text-success ml-2">
                                                <i class="fad fa-film"></i>
                                            </a>
                                            <a href="{{ route('course.edit', $course['id']) }}"
                                               title="{{ __('Edit') }}">
                                                <i class="fad fa-edit"></i>
                                            </a>
                                            <a href="javascript:;"
                                               v-on:click="dropItem('{{ route('course.destroy', $course['id']) }}')"
                                               title="{{ __('Drop') }}"
                                               class="text-danger mr-2">
                                                <i class="fad fa-trash-alt"></i>
                                            </a>
                                        </td>
                                        @empty
                                            <td colspan="8" class="text-center">No Course!</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-center">
                        {{ $courses->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
