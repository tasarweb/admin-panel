@extends('layouts.main')

@section('content')
    <?php $edit = isset($episode) ?>
    <div class="container-fluid">
        <form method="post" action="{{ $edit ? route('episode.update', $episode['id']) : route('episode.store') }}"
              enctype="multipart/form-data">
            <div class="row">
                <div class="col col-12 col-md-8 col-lg-9">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @csrf
                    @if($edit)
                        @method('PUT')
                    @endif
                    <input type="hidden" name="parent_id" value="{{ $course_id }}"/>
                    <div class="card">
                        <div class="card-header text-center bg-secondary text-light">
                            <strong>{{ $edit ? "Edit episode -> {$episode['title']}" : 'Create episode' }}</strong>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-12 col-md-6">
                                    <div class="row">
                                        <label for="title"
                                               class="col-12 col-form-label text-md-right">{{ __('Title') }}</label>
                                        <div class="col-12">
                                            <input id="title" type="text"
                                                   class="form-control @error('title') is-invalid @enderror"
                                                   name="title"
                                                   value="{{ old('title') }}" required>

                                            @error('title')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="row">
                                        <label for="slug"
                                               class="col-12 col-form-label text-md-right">{{ __('Slug') }}</label>
                                        <div class="col-12">
                                            <input id="slug" type="text"
                                                   class="form-control @error('slug') is-invalid @enderror" name="slug"
                                                   value="{{ old('slug') }}" required>

                                            @error('slug')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="description"
                                       class="col-12 col-form-label text-md-right">{{ __('Description') }}</label>
                                <div class="col-12">
                                    <textarea id="description" name="description"
                                              class="form-control @error('description') is-invalid @enderror">{{ old('description') }}</textarea>

                                    @error('description')
                                    <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                    @enderror
                                </div>
                            </div>
                        </div> <!-- @card-body -->
                    </div>
                </div>
                <div class="col col-12 col-md-4 col-lg-3">
                    <div class="card">
                        <div class="card-header bg-secondary">
                            <div class="text-left">
                                <button class="btn btn-success" type="submit">
                                    {{ $edit ? __('Update') : __('Save') }}
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" id="is_free" class="custom-control-input" name="is_free">
                                <label class="custom-control-label" for="is_free">Free</label>
                            </div>
                        </div>
                    </div> <!-- @card -->

                    <div class="card mt-2">
                        <div class="card-header">
                            {{ __('Video') }}
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <input type="file" class="form-control-file" name="video" id="video" accept="video/mp4,video/x-m4v,video/*"/>
                            </div>
                            <div id="video-div">
                                @if($edit && $episode->getFirstMediaFullUrl())
				   <video width="320" height="240" controls id="courseVideo">
				  	<source src="{{ $episode->getFirstMediaFullUrl() }}" type="video/mp4">
					Your browser does not support the video tag.
				   </video>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/tinymce.min.js') }}"></script>
    <script>
        tinymce.init({
            selector: '#description',
            height: 500,
            directionality: 'rtl',
            language: 'fa_IR',
            // content_css: '/css/font.css',
            path_absolute: "/",
            plugins: "code print preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern",
            toolbar: [
                "formatselect fontsizeselect bold italic underline strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify",
                "numlist bullist outdent indent | removeformat | codesample | image | fullscreen"
            ],
            extended_valid_elements: "svg[*],defs[*],pattern[*],desc[*],metadata[*],g[*],mask[*],path[*],line[*],marker[*],rect[*],circle[*],ellipse[*],polygon[*],polyline[*],linearGradient[*],radialGradient[*],stop[*],image[*],view[*],text[*],textPath[*],title[*],tspan[*],glyph[*],symbol[*],switch[*],use[*]",
            image_advtab: true,
            relative_urls: false,
            remove_script_host: false,
            external_filemanager_path: "/tasar-admin/filemanager/",
            filemanager_title: "Filemanager",
            filemanager_user: "{{ $edit ? $course->user->id : '' }}",
            external_plugins: {"filemanager": "/js/filemanager.min.js"}
        });
    </script>
    <script>
	document.addEventListener('DOMContentLoaded', function () {
		function readURL(input) {
		  if (input.files && input.files[0]) {
		    var reader = new FileReader();
		    
		    reader.onload = function(e) {
			if ($("#courseVideo").length) {
				$("#courseVideo").attr("src", e.target.result);
			} else {
				let video = $('<video width="240" height="240" controls id="courseVideo"><source src="" type="video/mp4">Your browser does not support the video tag.</video>');
				video.attr('src', e.target.result);
				video.appendTo('#video-div');
			}
		    }
		    
		    reader.readAsDataURL(input.files[0]);
		  }
		}

		$("#video").change(function (){
			readURL(this);
			$("#courseVideo")[0].load();
		});
	});
    </script>
@endsection
