@extends('layouts.main')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col col-8">
                <div class="card">
                    <div class="card-header">
                        @php
                            $courseFormat = floor($course['duration'] / 60) > 60 ? 'H:i:s' : 'i:s';
                            $courseDuration = gmdate($courseFormat, $course['duration']);
                        @endphp
                        <div class="row align-items-center">
                            <div class="col col-10">
                                <strong>
                                    {{ $course['title'] }} <span
                                        class="badge badge-secondary">{{ $courseDuration }}</span>
                                </strong>
                            </div>
                            <div class="col col-2 text-left">
                                <a href="{{ route('course.index') }}" title="{{ __('Back to list') }}"
                                   class="text-secondary ml-1">
                                    <i class="fad fa-share"></i>
                                </a>
                                <a href="{{ route('episode.create', ['course' => $course['id']]) }}"
                                   title="{{ __('Add new episode') }}" class="text-success ml-1">
                                    <i class="fad fa-video-plus"></i>
                                </a>
                                <a href="{{ route('course.edit', $course['id']) }}" title="{{ __('Edit course') }}">
                                    <i class="fad fa-edit"></i>
                                </a>
                            </div>
                        </div><!-- @row -->
                    </div><!-- @card-header -->
                    <div class="card-body">
                        <div class="accordion" id="accordionEpisodes">
                            @forelse($course['episodes'] as $episode)
                                <div class="card">
                                    <div class="card-header" id="heading-{{ $episode['id'] }}">
                                        <div class="row align-items-center">
                                            <div class="col col-10">
                                                @php
                                                    $format = floor($episode['duration'] / 60) > 60 ? 'H:i:s' : 'i:s';
                                                    $duration = gmdate($format, $episode['duration']);
                                                @endphp
                                                <h2 class="mb-0">
                                                    <button
                                                        class="btn btn-link btn-block text-right @if(!$loop->first) collapsed @endif"
                                                        type="button" data-toggle="collapse"
                                                        data-target="#collapse-{{ $episode['id'] }}"
                                                        aria-expanded="@if(!$loop->first) true @else false @endif"
                                                        aria-controls="collapse-{{ $episode['id'] }}">
                                                        {{ $episode['title'] }} <span
                                                            class="badge badge-secondary">{{ $duration }}</span>
                                                        @if($episode['is_free']) <span
                                                            class="badge badge-success">Free</span> @endif
                                                    </button>
                                                </h2>
                                            </div>

                                            <div class="col col-2 text-left">
                                                <a href="javascript:;" title="{{ __('Play') }}"
                                                   class="text-success ml-1" data-toggle="modal" data-target=".video-modal" data-episode="{{ $episode['id'] }}">
                                                    <i class="fad fa-play"></i>
                                                </a>
                                                <a href="{{ route('episode.edit', $episode['id']) }}"
                                                   title="{{ __('Edit') }}">
                                                    <i class="fad fa-edit"></i>
                                                </a>
                                                <a href="javascript:;"
                                                   v-on:click="dropItem('{{ route('episode.destroy', $episode['id']) }}')"
                                                   title="{{ __('Drop') }}"
                                                   class="text-danger mr-1">
                                                    <i class="fad fa-trash-alt"></i>
                                                </a>
                                            </div>

                                        </div>
                                    </div>

                                    <div id="collapse-{{ $episode['id'] }}"
                                         class="collapse @if($loop->first) show @endif"
                                         aria-labelledby="heading-{{ $episode['id'] }}"
                                         data-parent="#accordionEpisodes">
                                        <div class="card-body">
                                            {!! $episode['description'] !!}
                                        </div>
                                    </div>
                                </div>
                            @empty
                                <strong>No Episode!</strong>
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="modal fade video-modal" tabindex="-1" role="dialog" aria-labelledby="videoModal"
         aria-hidden="true" id="videoModal">
        <div class="modal-dialog" role="document" style="width:60vw;max-width:unset">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('Video episode') }}</h5>
                    <div class="text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                <div class="modal-body text-center">
                    <video width="320" height="240" controls id="courseVideoOnline">
		  	<source src="" type="video/mp4">
			Your browser does not support the video tag.
		   </video>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
	<script>

		document.addEventListener('DOMContentLoaded', function () {
			$.ajaxSetup({
				headers: {
		    			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
		    			'Authorization': 'Bearer {{ $_COOKIE["auth-token"] }}'
				}
	    		});
			$('#videoModal').on('show.bs.modal', function (e) {
				let episode_id = e.relatedTarget.attributes['data-episode'].value;
				let url = "{{ config('admin.episode_download_url') }}";
				$.ajax({
				  type: "POST",
				  //url: "{{ route('get.video') }}",
				  url: url.replace('%s', episode_id),
				  data: null,
				}).done(function (response, textStatus, jqXHR){
					//let data = $.parseJSON(response);
					$('#courseVideoOnline source').attr('src', response.url);
					$("#courseVideoOnline")[0].load();
				});
			});          
		});
	</script>
@endsection
