@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col col-12">
            <div class="card">
                <div class="card-header bg-secondary text-light text-center">
                    <strong>Dashboard</strong>
                </div>
                <div class="card-body text-center">
                    Welcome to admin dashboard
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
