@extends('admin.layouts.main')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-2"></div>
            <div class="col col-8">
                <div class="card">
                    <div class="card-header text-center">
                        <strong>Permissions</strong>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th><label><input type="checkbox" id="select-all-checkboxes"></label></th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Roles</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($permissions as $permission)
                                    <tr>
                                        <td>
                                            <label><input type="checkbox" value="{{ $permission->id }}[]"></label>
                                        </td>
                                        <td>{{ $permission->name }}</td>
                                        <td>
                                            @foreach($permission->roles as $role)
                                                @if(!$loop->first)
                                                    ,
                                                @endif
                                                {{ $role->name }}
                                            @endforeach
                                        </td>
                                        <td class="text-center">
                                            <a href="{{ route('permission.edit', $permission->id) }}"
                                               title="{{ __('Edit') }}">
                                                <i class="fad fa-edit"></i>
                                            </a>
                                            <a href="{{ route('permission.destroy', $permission->id) }}"
                                               title="{{ __('Drop') }}"
                                               class="text-danger mr-2">
                                                <i class="fad fa-trash-alt"></i>
                                            </a>
                                        </td>
                                        @empty
                                            <td colspan="4" class="text-center">No Permission!</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-center">
                        {{ $permissions->links() }}
                    </div>
                </div>
            </div>
            <div class="col col-2"></div>
        </div>
    </div>
@endsection
