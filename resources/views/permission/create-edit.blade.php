@extends('admin.layouts.main')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-2"></div>
            <div class="col col-8">
                <form method="post" action="{{ route('permission.store') }}">
                    @csrf
                    <div class="card">
                        <div class="card-header text-center">
                            <strong>Create Permission</strong>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="name"
                                       class="col-4 col-form-label text-md-right">{{ __('Permission Name') }}</label>
                                <div class="col-6">
                                    <input id="name" type="text"
                                           class="form-control @error('name') is-invalid @enderror" name="name"
                                           value="{{ old('name') }}" required>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="guard_name"
                                       class="col-4 col-form-label text-md-right">{{ __('Guard Name') }}</label>
                                <div class="col-6">
                                    <input id="guard_name" type="text"
                                           class="form-control @error('guard_name') is-invalid @enderror"
                                           name="guard_name"
                                           value="{{ old('guard_name') ?? 'web' }}">

                                    @error('guard_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="roles" class="col-4 col-form-label text-md-right">{{ __('Roles') }}</label>
                                <div class="col-6">
                                    <select id="roles" name="roles[]" class="form-control" multiple>
                                        <option selected value="null" disabled>No Roles</option>
                                        @foreach($roles as $role)
                                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('roles')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div> <!-- @card-body -->
                        <div class="card-footer text-center">
                            <button class="btn btn-success btn-lg" type="submit">{{ __('Save Permission') }}</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col col-2"></div>
        </div>
    </div>
@endsection
