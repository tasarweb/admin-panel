@if(count($item['children']) > 0)
    @foreach($item['children'] as $subCat)
        <option
            value="{{ $subCat['id'] }}" {{ $edit ? $category->parent === $subCat['id'] ? 'selected' : '' : '' }}>
            {{ $subCat['title'] }}
        </option>
        @include('category.sub-category-option', ['item' => $subCat, 'items' => $items])
    @endforeach
@endif
