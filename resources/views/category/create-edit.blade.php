@extends('layouts.main')

@section('content')
    <?php $edit = isset($category) ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col col-2"></div>
            <div class="col col-8">

                <form method="post"
                      action="{{ $edit ? route('category.update', $category['id']) : route('category.store') }}">
                    @csrf
                    @if($edit)
                        @method('PUT')
                    @endif
                    <div class="card">
                        <div class="card-header text-center">
                            <strong>{{ $edit ? "Edit Category -> {$category['title']}" : 'Create Category' }}</strong>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label text-md-right">{{ __('Name') }}</label>
                                <div class="col-6">
                                    <input id="name" type="text"
                                           class="form-control @error('title') is-invalid @enderror" name="title"
                                           value="{{ old('title') ?? $edit ? $category['title'] : '' }}" required>

                                    @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="slug" class="col-4 col-form-label text-md-right">{{ __('Slug') }}</label>
                                <div class="col-6">
                                    <input id="slug" type="text"
                                           class="form-control @error('slug') is-invalid @enderror" name="slug"
                                           value="{{ old('slug') ?? $edit ? $category['slug'] : '' }}" required>

                                    @error('slug')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="parent"
                                       class="col-4 col-form-label text-md-right">{{ __('Parent') }}</label>
                                <div class="col-6">
                                    <select id="parent" name="parent" class="form-control">
                                        <option selected value="">No Parent</option>
                                            @foreach($categories as $category)
                                                <optgroup label="{{ $category['title'] }}">
                                                    <option
                                                        value="{{ $category['id'] }}" {{ $edit ? $category->parent === $cat->id ? 'selected' : '' : '' }}>
                                                        {{ $category['title'] }}
                                                    </option>
                                                    @include('category.sub-category-option', ['item' => $category, 'items' => $categories])
                                                </optgroup>
                                            @endforeach
                                      
                                    </select>
                                    @error('parent')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div> <!-- @card-body -->
                        <div class="card-footer text-center">
                            <button class="btn btn-success btn-lg"
                                    type="submit">{{ $edit ? __('Update') : __('Save') }}</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col col-2"></div>
        </div>
    </div>
@endsection
