@extends('layouts.main')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-2"></div>
            <div class="col col-8">
                <div class="card">
                    <div class="card-header text-center">
                        <strong>Categories</strong>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead class="thead-dark">
                                <tr>
                                    <th>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="all-items" class="custom-control-input"
                                            v-model="allItems">
                                            <label class="custom-control-label" for="all-items"></label>
                                        </div>
                                    </th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Slug</th>
                                    <th class="text-center">Parent</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($categories as $category)
                                    <tr>
                                        <th class="checkbox">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" value="{{ $category['id'] }}"
                                                       name="category[]" id="{{ $category['id'] }}"
                                                       class="custom-control-input">
                                                <label class="custom-control-label" for="{{ $category['id'] }}"></label>
                                            </div>
                                        </th>
                                        <td>{{ $category['title'] }}</td>
                                        <td>{{ $category['slug'] }}</td>
                                        <td>{{ $category['parent'] }}</td>
                                        <td class="text-center">
                                            <a href="{{ route('category.edit', $category['id']) }}" title="{{ __('Edit') }}">
                                                <i class="fad fa-edit"></i>
                                            </a>
                                            <a href="javascript:;"  v-on:click="dropItem('{{ route('category.destroy', $category['id']) }}')"
                                               title="{{ __('Drop') }}"
                                               class="text-danger mr-2">
                                                <i class="fad fa-trash-alt"></i>
                                            </a>
                                        </td>
                                        @empty
                                            <td colspan="5" class="text-center">No Category!</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-center">
                        {{ $categories->links() }}
                    </div>
                </div>
            </div>
            <div class="col col-2"></div>
        </div>
    </div>
@endsection
