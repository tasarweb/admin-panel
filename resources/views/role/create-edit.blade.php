@extends('admin.layouts.main')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-2"></div>
            <div class="col col-8">
                <form method="post" action="{{ route('role.store') }}">
                    @csrf
                    <div class="card">
                        <div class="card-header text-center">
                            <strong>Create Role</strong>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="name" class="col-4 col-form-label text-md-right">{{ __('Name') }}</label>
                                <div class="col-6">
                                    <input id="name" type="text"
                                           class="form-control @error('name') is-invalid @enderror" name="name"
                                           value="{{ old('name') }}" required>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="guard_name" class="col-4 col-form-label text-md-right">{{ __('Guard Name') }}</label>
                                <div class="col-6">
                                    <input id="guard_name" type="text"
                                           class="form-control @error('guard_name') is-invalid @enderror" name="guard_name"
                                           value="{{ old('guard_name') ?? 'web' }}">

                                    @error('guard_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="permissions" class="col-4 col-form-label text-md-right">{{ __('Permissions') }}</label>
                                <div class="col-6">
                                    <select id="permissions" name="permissions[]" class="form-control" multiple>
                                        <option selected value="null" disabled>No Permission</option>
                                        @foreach($permissions as $permission)
                                            <option value="{{ $permission->id }}">{{ $permission->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('permissions')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div> <!-- @card-body -->
                        <div class="card-footer text-center">
                            <button class="btn btn-success btn-lg" type="submit">{{ __('Save Role') }}</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col col-2"></div>
        </div>
    </div>
@endsection
