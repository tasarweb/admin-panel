@extends('admin.layouts.main')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-2"></div>
            <div class="col col-8">
                <div class="card">
                    <div class="card-header text-center">
                        <strong>Roles</strong>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th><label><input type="checkbox" id="select-all-checkboxes"></label></th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Permissions</th>
                                    <th class="text-center">Users Count</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($roles as $role)
                                    <tr>
                                        <td>
                                            <label><input type="checkbox" value="{{ $role->id }}[]"></label>
                                        </td>
                                        <td>{{ $role->name }}</td>
                                        <td>
                                            @foreach($role->permissions as $permission)
                                                @if(!$loop->first)
                                                    ,
                                                @endif
                                                {{ $permission->name }}
                                            @endforeach
                                        </td>
                                        <td class="text-center">{{ $role->users->count() }}</td>
                                        <td class="text-center">
                                            <a href="{{ route('role.edit', $role->id) }}"
                                               title="{{ __('Edit') }}">
                                                <i class="fad fa-edit"></i>
                                            </a>
                                            <a href="{{ route('role.destroy', $role->id) }}"
                                               title="{{ __('Drop') }}"
                                               class="text-danger mr-2">
                                                <i class="fad fa-trash-alt"></i>
                                            </a>
                                        </td>
                                        @empty
                                            <td colspan="4" class="text-center">No Role!!!</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-2"></div>
        </div>
    </div>
@endsection
