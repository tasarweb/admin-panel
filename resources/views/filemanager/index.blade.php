@extends('admin.layouts.main')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <iframe src="{{ route('admin.filemanager.dialog') }}" width="100%"
                    style="width:95vw;height:85vh;border:none"></iframe>
        </div>
    </div>
@endsection
