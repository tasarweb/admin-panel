@extends('admin.layouts.main')

@section('content')
    <?php $edit = isset($post) ?>
    <div class="container-fluid">
        <form method="post" action="{{ $edit ? route('post.update', $post->id) : route('post.store') }}"
              enctype="multipart/form-data">
            <div class="row">
                <div class="col col-12 col-md-8 col-lg-9">
                    @csrf
                    @if($edit)
                        @method('PUT')
                    @endif
                    <div class="card">
                        <div class="card-header text-center bg-secondary text-light">
                            <strong>{{ $edit ? "Edit Post -> {$post->title}" : 'Create Post' }}</strong>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-12 col-md-6">
                                    <div class="row">
                                        <label for="title"
                                               class="col-12 col-form-label text-md-right">{{ __('Title') }}</label>
                                        <div class="col-12">
                                            <input id="title" type="text"
                                                   class="form-control @error('title') is-invalid @enderror"
                                                   name="title"
                                                   value="{{ old('title') ?? $edit ? $post->title : '' }}" required>

                                            @error('title')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="row">
                                        <label for="slug"
                                               class="col-12 col-form-label text-md-right">{{ __('Slug') }}</label>
                                        <div class="col-12">
                                            <input id="slug" type="text"
                                                   class="form-control @error('slug') is-invalid @enderror" name="slug"
                                                   value="{{ old('slug') ?? $edit ? $post->slug : '' }}" required>

                                            @error('slug')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="content"
                                       class="col-12 col-form-label text-md-right">{{ __('Content') }}</label>
                                <div class="col-12">
                                    <textarea id="content" name="content"
                                              class="form-control @error('content') is-invalid @enderror">{{ old('content') ?? $edit ? $post->content : '' }}</textarea>

                                    @error('content')
                                    <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                    @enderror
                                </div>
                            </div>
                        </div> <!-- @card-body -->
                    </div>
                </div>
                <div class="col col-12 col-md-4 col-lg-3">
                    <div class="card">
                        <div class="card-header bg-secondary">
                            <div class="text-left">
                                <button class="btn btn-success" type="submit">
                                    {{ $edit ? __('Update') : __('Save') }}
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="category" class="col-12 col-form-label text-md-right">
                                    {{ __('Category') }}
                                </label>
                                <div class="col col-12">
                                    <?php $cats = $categories->groupBy('parent'); ?>
                                    <select id="category" name="category_id"
                                            class="form-control @error('category_id') is-invalid @enderror">
                                        @if(!$edit || $edit && count($post->categories) === 0)
                                            <option selected disabled>No Category Selected!</option>
                                        @endif
                                        @if($categories->count() > 0)
                                            @foreach($cats[''] as $cat)
                                                <optgroup label="{{ $cat->name }}">
                                                    <option
                                                        value="{{ $cat->id }}"
                                                        {{ $edit ? (count($post->categories) > 0 && $post->categories[0]->id) === $cat->id ? 'selected' : '' : '' }}>
                                                        {{ $cat->name }}
                                                    </option>
                                                    @include('admin.category.sub-category-option', ['item' => $cat, 'items' => $cats, 'category' => $edit ? $post : ''])
                                                </optgroup>
                                            @endforeach
                                        @endif
                                    </select>
                                    @error('category_id')
                                    <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col col-12 col-md-6">
                                    <div class="row">
                                        <label for="type" class="col-12 col-form-label text-md-right">
                                            {{ __('Type') }}
                                        </label>
                                        <div class="col col-12">
                                            <select name="type" id="type"
                                                    class="form-control @error('type') is-invalid @enderror">
                                                @foreach($types as $key => $type)
                                                    <option
                                                        value="{{ $key }}" {{ $edit ? $post->type === strtolower($key) ? 'selected' : '' : '' }}>
                                                        {{ $type }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('type')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-12 col-md-6">
                                    <div class="row">
                                        <label for="status" class="col-12 col-form-label text-md-right">
                                            {{ __('Status') }}
                                        </label>
                                        <div class="col col-12">
                                            <select name="status" id="status"
                                                    class="form-control @error('type') is-invalid @enderror">
                                                @foreach($status as $key => $value)
                                                    <option
                                                        value="{{ $key }}" {{ $edit ? $post->status === strtolower($key) ? 'selected' : '' : $key === 'DRAFT' ? 'selected' : '' }}>
                                                        {{ $value }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('status')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- @card -->
                    <div class="card mt-2">
                        <div class="card-header">
                            {{ __('Tags') }}
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <select-tags :all-tags="{{ $tags }}"
                                             :default-tags="{{ $edit ? $post->tags()->pluck('name') : 'null' }}"></select-tags>
                            </div>
                        </div>
                    </div>

                    <div class="card mt-2">
                        <div class="card-header">
                            <div class="row">
                                <div class="col col-6">
                                    {{ __('Image') }}
                                </div>
                                <div class="col col-6 text-left">
                                    <button type="button" class="btn btn-outline-dark" data-toggle="modal"
                                            data-target=".filemanager-modal">
                                        {{ __('Select Image') }}
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="image-div">
                                @if($edit && $post->getFirstMediaFullUrl())
                                    <img src="{{ $post->getFirstMediaFullUrl('images', 'thumb') }}"
                                         alt="{{ $post->getFirstMediaName() }}"
                                         class="rounded img-thumbnail" id="postImage"/>
                                @endif
                            </div>
                            <input type="hidden" name="image_id" id="imageId">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="modal fade filemanager-modal" tabindex="-1" role="dialog" aria-labelledby="filemanagerModal"
         aria-hidden="true" id="filemanagerModal">
        <div class="modal-dialog" role="document" style="width:90vw;max-width:unset">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('Select image') }}</h5>
                    <div class="text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                <div class="modal-body">
                    <iframe src="{{ route('admin.filemanager.dialog') }}?type=1&select_post_image=1" width="100%"
                            style="border:none;height:75vh;"></iframe>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/tinymce.min.js') }}"></script>
    <script>
        tinymce.init({
            selector: '#content',
            height: 500,
            directionality: 'rtl',
            language: 'fa_IR',
            // content_css: '/css/font.css',
            path_absolute: "/",
            plugins: "code print preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern",
            toolbar: [
                "formatselect fontsizeselect bold italic underline strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify",
                "numlist bullist outdent indent | removeformat | codesample | image | fullscreen"
            ],
            extended_valid_elements: "svg[*],defs[*],pattern[*],desc[*],metadata[*],g[*],mask[*],path[*],line[*],marker[*],rect[*],circle[*],ellipse[*],polygon[*],polyline[*],linearGradient[*],radialGradient[*],stop[*],image[*],view[*],text[*],textPath[*],title[*],tspan[*],glyph[*],symbol[*],switch[*],use[*]",
            image_advtab: true,
            relative_urls: false,
            remove_script_host: false,
            external_filemanager_path: "/tasar-admin/filemanager/",
            filemanager_title: "Filemanager",
            filemanager_user: "{{ $edit ? $post->user->id : '' }}",
            external_plugins: {"filemanager": "/js/filemanager.min.js"}
        });
    </script>
    <script>
        window.addEventListener('message', function receiveMessage(event) {
            if (event.data.sender === 'insert-post-image') {
                $('#filemanagerModal').modal('hide');
                $('#imageId').val(event.data.id);
                if ($("#postImage").length) {
                    $("#postImage").attr("src", event.data.url);
                } else {
                    let img = $('<img id="postImage">');
                    img.attr('src', event.data.url);
                    img.appendTo('#image-div');
                }
            }
        }, false);
    </script>
@endsection
