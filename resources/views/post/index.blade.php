@extends('admin.layouts.main')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col col-8">
                <div class="card">
                    <div class="card-header text-center">
                        <strong>Posts</strong>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead class="thead-dark">
                                <tr>
                                    <th>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="all-items" class="custom-control-input"
                                            v-model="allItems">
                                            <label class="custom-control-label" for="all-items"></label>
                                        </div>
                                    </th>
                                    <th class="text-center">Title</th>
                                    <th class="text-center">Author</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Type</th>
                                    <th class="text-center">Featured</th>
                                    <th class="text-center">Published</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($posts as $post)
                                    <tr>
                                        <th class="checkbox">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" value="{{ $post->id }}"
                                                       name="category[]" id="{{ $post->id }}"
                                                       class="custom-control-input">
                                                <label class="custom-control-label" for="{{ $post->id }}"></label>
                                            </div>
                                        </th>
                                        <td>{{ $post->title }}</td>
                                        <td>
                                            <a href="{{ route('api.user.show', $post->author['username']) }}" target="_blank">
                                                {{ $post->author['username'] }}
                                            </a>
                                        </td>
                                        <td>{{ $post->status }}</td>
                                        <td>{{ $post->type }}</td>
                                        <td>{{ $post->featured }}</td>
                                        <td>{{ $post->updated_at->diffForHumans() }}</td>
                                        <td class="text-center">
                                            <a href="{{ route('post.edit', $post->id) }}" title="{{ __('Edit') }}">
                                                <i class="fad fa-edit"></i>
                                            </a>
                                            <a href="javascript:;"  v-on:click="dropItem('{{ route('post.destroy', $post->id) }}')"
                                               title="{{ __('Drop') }}"
                                               class="text-danger mr-2">
                                                <i class="fad fa-trash-alt"></i>
                                            </a>
                                        </td>
                                        @empty
                                            <td colspan="8" class="text-center">No Category!</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-center">
                        {{ $posts   ->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
