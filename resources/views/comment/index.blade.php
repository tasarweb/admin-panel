@extends('admin.layouts.main')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col col-8">
                <div class="card">
                    <div class="card-header text-center">
                        <strong>Comments</strong>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead class="thead-dark">
                                <tr>
                                    <th>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" id="all-items" class="custom-control-input"
                                                   v-model="allItems">
                                            <label class="custom-control-label" for="all-items"></label>
                                        </div>
                                    </th>
                                    <th class="text-center">Author</th>
                                    <th class="text-center">For</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Created</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($comments as $comment)
                                    <tr>
                                        <th class="checkbox">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" value="{{ $comment->id }}"
                                                       name="category[]" id="{{ $comment->id }}"
                                                       class="custom-control-input">
                                                <label class="custom-control-label" for="{{ $comment->id }}"></label>
                                            </div>
                                        </th>
                                        <td>{{ $comment->author_name }}</td>
                                        <td>
                                            <a href="{{ route("api.{$comment->for['type']}.show", $comment->for['slug']) }}" target="_blank">
                                                {{ $comment->for['title'] }}
                                            </a>
                                        </td>
                                        <td>{{ $comment->approved ? __('messages.published') : __('messages.on_published') }}</td>
                                        <td>{{ $comment->created_at->diffForHumans() }}</td>
                                        <td class="text-center">
                                            <a href="{{ route('comment.edit', $comment->id) }}"
                                               title="{{ __('Edit') }}">
                                                <i class="fad fa-edit"></i>
                                            </a>
                                            <a href="javascript:;"
                                               v-on:click="dropItem('{{ route('comment.destroy', $comment->id) }}')"
                                               title="{{ __('Drop') }}"
                                               class="text-danger mr-2">
                                                <i class="fad fa-trash-alt"></i>
                                            </a>
                                        </td>
                                        @empty
                                            <td colspan="6" class="text-center">No Comment!</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-center">
                        {{ $comments->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
