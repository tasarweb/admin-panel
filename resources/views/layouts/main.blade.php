<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    @notifyCss
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @yield('styles')
</head>
<body>
<section id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>
            <?php $currentUrl = \URL::current(); ?>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown {{ ($currentUrl == route('category.index') || $currentUrl == route('category.create')) ? 'active' : '' }}">
                        <a id="categoryDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown">
                            {{ __('Category') }}
                            <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="categoryDropdown">
                            <a class="dropdown-item {{ ($currentUrl == route('category.index')) ? 'active' : ''  }}"
                               href="{{ route('category.index') }}">
                                {{ __('List') }}
                            </a>
                            <a class="dropdown-item {{ ($currentUrl == route('category.create')) ? 'active' : ''  }}"
                               href="{{ route('category.create') }}">
                                {{ __('Create') }}
                            </a>
                        </div>
                    </li>
                    
                    <li class="nav-item dropdown {{ ($currentUrl == route('user.index') || $currentUrl == route('user.create')) ? 'active' : '' }}">
                        <a id="userDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown">
                            {{ __('Users') }}
                            <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                            <a class="dropdown-item {{ $currentUrl == route('user.index') ? 'active' : '' }}" href="{{ route('user.index') }}">
                                {{ __('List') }}
                            </a>
                            <a class="dropdown-item {{ $currentUrl == route('user.create') ? 'active' : '' }}" href="{{ route('user.create') }}">
                                {{ __('Create') }}
                            </a>
                        </div>
                    </li>
                    
                    <li class="nav-item dropdown {{ ($currentUrl == route('course.index') || $currentUrl == route('course.create')) ? 'active' : '' }}">
                        <a id="courseDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown">
                            {{ __('Course') }}
                            <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="courseDropdown">
                            <a class="dropdown-item {{ $currentUrl == route('course.index') ? 'active' : '' }}" href="{{ route('course.index') }}">
                                {{ __('List') }}
                            </a>
                            <a class="dropdown-item {{ $currentUrl == route('course.create') ? 'active' : '' }}" href="{{ route('course.create') }}">
                                {{ __('Create') }}
                            </a>
                        </div>
                    </li>

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item dropdown">
                        <a id="accountDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->username }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-left" aria-labelledby="accountDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <main class="py-4">
        @yield('content')
    </main>
</section>
@include('notify::messages')
@notifyJs
<script src="{{ asset('js/sweetalert2.all.min.js') }}"></script>
@yield('scripts')
</body>
</html>
