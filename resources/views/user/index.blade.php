@extends('layouts.main')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-2"></div>
            <div class="col col-8">
                <div class="card">
                    <div class="card-header text-center">
                        <strong>Users</strong>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th><label><input type="checkbox" id="select-all-checkboxes"></label></th>
                                    <th class="text-center">First Name</th>
                                    <th class="text-center">Last Name</th>
                                    <th class="text-center">Username</th>
                                    <th class="text-center">Email</th>
                                    <th class="text-center">Mobile</th>
                                    <th class="text-center">Role</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($users as $user)
                                    <tr>
                                        <td>
                                            <label><input type="checkbox" value="{{ $user->id }}[]"></label>
                                        </td>
                                        <td>{{ $user->first_name }}</td>
                                        <td>{{ $user->last_name }}</td>
                                        <td class="text-center">{{ $user->username }}</td>
                                        <td class="text-center">{{ $user->email }}</td>
                                        <td class="text-center">{{ $user->phone }}</td>
                                        <td class="text-center">
                                            @foreach($user->roles as $role)
                                                @if(!$loop->first)
                                                    ,
                                                @endif
                                                {{ $role->name }}
                                            @endforeach
                                        </td>
                                        <td class="text-center">
                                            <a href="{{ route('user.edit', $user->id) }}" title="{{ __('Edit') }}">
                                                <i class="fad fa-user-edit"></i>
                                            </a>
                                            <a href="{{ route('user.destroy', $user->id) }}" title="{{ __('Drop') }}"
                                               class="text-danger mr-2">
                                                <i class="fad fa-trash-alt"></i>
                                            </a>
                                            <a href="#" title="{{ __('See Profile') }}"
                                               class="text-secondary mr-2" target="_blank">
                                                <i class="fad fa-eye"></i>
                                            </a>
                                        </td>
                                        @empty
                                            <td colspan="8" class="text-center">No User!!!</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-center">
                        {{ $users->links() }}
                    </div>
                </div>
            </div>
            <div class="col col-2"></div>
        </div>
    </div>
@endsection
