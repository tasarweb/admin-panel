require('./bootstrap');
import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css';

window.Vue = require('vue');
//require('../assets/vendor/MediaManager/js/manager');
Vue.component('v-select', vSelect);
Vue.component('select-tags', require('./components/SelectTags.vue').default);

const app = new Vue({
    el: '#app',
    data: {
        allItems: false,
    },
    methods: {
        dropItem(url) {
            Swal.fire({
                title: "Delete this?",
                text: "Are you sure? You won't be able to revert this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                confirmButtonText: "Yes, Delete it!",
                closeOnConfirm: true
            }).then((result) => {
                if (result.value) {
                    axios.delete(url)
                        .then(response => {
                            if(response.data.ok) {
                                Swal.fire(
                                    'Deleted!',
                                    response.data.message,
                                    'success'
                                ).then((result) => {
                                    location.reload()
                                });
                            }
                        });
                }
            })
        }
    }
});
