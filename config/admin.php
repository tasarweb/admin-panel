<?php

return [
    'api_url' => env('API_URL'),
    'api_version' => env('API_VERSION'),
    'login_url' => env('API_URL') . '/login',
    'course_list_url' => env('API_URL') . '/' . env('API_VERSION') . '/course/admin/list',
    'course_categories_list_url' => env('API_URL') . '/' . env('API_VERSION') . '/course/category',
    'course_create_category_url' => env('API_URL') . '/' . env('API_VERSION') . '/course/category',
    'course_tags_list_url' => env('API_URL') . '/' . env('API_VERSION') . '/course/tag',
    'course_create_url' => env('API_URL') . '/' . env('API_VERSION') . '/course/create',
    'course_get_url' => env('API_URL') . '/' . env('API_VERSION') . '/course/%s',
    'episode_download_url' => env('API_URL') . '/' . env('API_VERSION') . '/course/%s/download',
];
